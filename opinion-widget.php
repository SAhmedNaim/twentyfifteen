<?php 

class OpinionWidget extends WP_Widget
{
	
	function __construct()
	{
		parent::__construct(
			'OpinionWidget',
			'Your Opinion',
			array(
				'description'	=> 'All about your opinion'
			)
		);
	}

	public function form($instance)
	{
		if(isset($instance['opinionTitleValue']))
		{
			$title = $instance['opinionTitleValue'];
		}
		else
		{
			$title = 'My Opinion';
		}

		if(isset($instance['opinionCompanyValue']))
		{
			$company = $instance['opinionCompanyValue'];
		}
		else
		{
			$company = 'My Opinion';
		}

		?>
		
		<p>
			<label for="<?php echo $this->get_field_id('opinionTitleValue'); ?>">Your Subject:</label>
			<input id="<?php echo $this->get_field_id('opinionTitleValue'); ?>" 
				name="<?php echo $this->get_field_name('opinionTitleValue'); ?>" 
				value="<?php echo $title; ?>" 
				type="text" class="widefat title" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('opinionCompanyValue'); ?>">Your Company:</label>
			<input id="<?php echo $this->get_field_id('opinionCompanyValue'); ?>" 
				name="<?php echo $this->get_field_name('opinionCompanyValue'); ?>" 
				value="<?php echo $company; ?>" 
				type="text" class="widefat title" />
		</p>

		<?php
	}

	public function widget($args, $instance)
	{
		if(isset($instance['opinionTitleValue']))
		{
			$title = $instance['opinionTitleValue'];
		}
		else
		{
			$title = 'My Opinion';
		}

		if(isset($instance['opinionCompanyValue']))
		{
			$company = $instance['opinionCompanyValue'];
		}
		else
		{
			$company = 'My Opinion';
		}

		echo $args['before_widget']
			.$args['before_title']
			.$title
			.$args['after_title']
			."<p>Comapny: ".$company."</p>"
			.$args['after_widget']
		;

		


	}
}

function widget_initialization()
{
	register_widget( 'OpinionWidget' );
}
add_action( 'widgets_init', 'widget_initialization' );
