<?php

class AuthorBio extends WP_Widget
{
	
	function __construct()
	{
		parent::__construct(
			'AuthorBio', 
			__('Author Widget', 'twentyfifteen'), 
			array(
				'description'	=> 'This is all about author'
			)
		);
	}

	public function widget($args, $instance)
	{
		$title 			= $instance['authorTitleValue'];
		$company		= $instance['authorCompanyValue'];
		$designation	= $instance['authorDesignationValue'];
		$photo			= $instance['authorPhotoValue'];

		$image_content = 	'<div class="author-photo">
			<img src="' . get_template_directory_uri().'/inc/photo.jpg' . '" alt="Image Missing"/>
		</div>';

		$widget_content = $args['before_widget']
							.$args['before_title']
							.'Name: ' . $title . '<br/>'
							.'Company: ' . $company . '<br/>'
							.'Designation: ' . $designation . '<br/>'
							.$image_content
							.$args['after_title']
							.$args['after_widget'];

		echo $widget_content;
	}

	public function form($instance)
	{
		
		$title 			= $instance['authorTitleValue'];
		$company		= $instance['authorCompanyValue'];
		$designation	= $instance['authorDesignationValue'];
		$photo			= $instance['authorPhotoValue'];
		
		?>
		
		<p>
			<label for="<?php echo $this->get_field_id('authorTitleValue'); ?>">Author Name:</label>
			<input id="<?php echo $this->get_field_id('authorTitleValue'); ?>" 
				name="<?php echo $this->get_field_name('authorTitleValue'); ?>" 
				value="<?php echo $title; ?>" 
				type="text" class="widefat title" placeholder="Enter Name Here..." />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('authorCompanyValue'); ?>">Author Company:</label>
			<input id="<?php echo $this->get_field_id('authorCompanyValue'); ?>" 
				name="<?php echo $this->get_field_name('authorCompanyValue'); ?>" 
				value="<?php echo $company; ?>" 
				type="text" class="widefat title" placeholder="Enter Company Here..." />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('authorDesignationValue'); ?>">Author Designation:</label>
			<input id="<?php echo $this->get_field_id('authorDesignationValue'); ?>" 
				name="<?php echo $this->get_field_name('authorDesignationValue'); ?>" 
				value="<?php echo $designation; ?>" 
				type="text" class="widefat title" placeholder="Enter Designation Here..." />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('authorPhotoValue'); ?>">Photo:</label>
			<input id="<?php echo $this->get_field_id('authorPhotoValue'); ?>" 
				name="<?php echo $this->get_field_name('authorPhotoValue'); ?>" 
				value="<?php echo $photo; ?>" 
				type="text" class="widefat image1" placeholder="Click to upload your photo..." />
		</p>
		<button class="imageUpload1">Upload Image</button>


		<?php
	}

}

function author_bio_init()
{
	register_widget( 'AuthorBio' );
}
add_action( 'widgets_init', 'author_bio_init' );






function photo_upload_option()
{
	wp_register_script( 'uploadPhoto', get_template_directory_uri().'/js/upload-photo.js', 
		array('jquery', 'media-upload', 'thickbox') 
	);

	wp_enqueue_script( 'media-upload' );
	wp_enqueue_script( 'thickbox' );

	wp_enqueue_style( 'thickbox' );
	wp_enqueue_script( 'uploadPhoto' );

	
	
}
add_action( 'admin_enqueue_scripts', 'photo_upload_option' );
