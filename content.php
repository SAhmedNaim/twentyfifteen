<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php
		// Post thumbnail.
		twentyfifteen_post_thumbnail();
	?>

	<header class="entry-header">
		<?php
			if ( is_single() ) :
				the_title( '<h1 class="entry-title">', '</h1>' );
			else :
				the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
			endif;
		?>

		

	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
			/* translators: %s: Name of current post */
			the_content( sprintf(
				__( 'Continue reading %s', 'twentyfifteen' ),
				the_title( '<span class="screen-reader-text">', '</span>', false )
			) );

			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentyfifteen' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' </span>%',
				'separator'   => '<span class="screen-reader-text">, </span>',
			) );
		?>
		
		<?php $prefix = '_fifteen_'; ?>

		<h2> Framework:
			<?php
				echo get_post_meta( get_the_ID() , $prefix . 'framework' , true );
			?>
		</h2>

		<h2> Nickname:
			<?php
				echo get_post_meta( get_the_ID() , $prefix . 'nickname' , true );
			?>
		</h2>

		<h2> Email:
			<?php
				echo get_post_meta( get_the_ID() , $prefix . 'email' , true );
			?>
		</h2>

		<h2> Income:
			<?php
				echo get_post_meta( get_the_ID() , $prefix . 'income' , true );
			?>
		</h2>

		<p> Address:
			<?php
				echo get_post_meta( get_the_ID() , $prefix . 'address' , true );
			?>
		</p>

		<h2> Timezone:
			<?php
				echo get_post_meta( get_the_ID() , $prefix . 'timezone' , true );
			?>
		</h2>

		<h2> Birthday:
			<?php
				echo get_post_meta( get_the_ID() , $prefix . 'birthday' , true );
			?>
		</h2>

		<h2 style="
			color: <?php
				echo get_post_meta( get_the_ID() , $prefix . 'color' , true );
			?>
		"> Color: 
			<?php
				echo get_post_meta( get_the_ID() , $prefix . 'color' , true );
			?>
		</h2>

		<h2> Gender:
			<?php
				$gender = get_post_meta( get_the_ID() , $prefix . 'gender' , true );
				if($gender == '1') : ?>
					Chele <?php
				else : ?>
					Meye <?php
				endif;
			?>
		</h2>

		<h2>Category: 
			<?php 
				echo get_post_meta(get_the_ID(), $prefix . 'my-category', true);
			?>
		</h2>

		<h2>Capital: 
			<?php 
				$capital = get_post_meta(get_the_ID(), $prefix . 'capital-city', true);

				if($capital == '1')
				{
					echo 'Dhaka';
				}
				else if($capital == '2')
				{
					echo 'Kolkata';
				}
				else if($capital == '3')
				{
					echo 'Mumbai';
				}
				else if($capital == '4')
				{
					echo 'Thailand';
				}
			?>
		</h2>

		<h2>Tag: 
			<?php 
				$tag = get_post_meta(get_the_ID(), $prefix . 'my-tag', true);
				$tag_id = get_tag($tag[0]);
				echo $tag_id->name;
			?>
		</h2>

		<h2>Languages: 
			<?php 
				$languages = get_post_meta(get_the_ID(), $prefix . 'languages', true);

				foreach ($languages as $value) 
				{
					echo $value . ', ';
				}
			?>
		</h2>

		<h2>Your category: 
			<?php 
				$category = get_post_meta(get_the_ID(), $prefix . 'your_category', true);

				echo $category;
			?>
		</h2>

		<h2>Publish content: 
			<?php 
				echo get_post_meta( get_the_ID(), $prefix . 'publish_content', true );
			?>
		</h2>

		<h2>Another image
			<img src="<?php echo get_post_meta( get_the_ID(), $prefix . 'another_image', true ); ?>" alt="Image Missing"/>
		</h2>

		<h2>
			<?php
				$gallery = get_post_meta(get_the_ID(), $prefix . 'gallery', true);

				echo '<ul>';
				foreach($gallery as $value)
				{
					echo '<li><img src="' . $value . '" alt="Missing"/></li>';
				}
				echo '</ul>';
			?>
		</h2>

	</div><!-- .entry-content -->

	<?php
		// Author bio.
		if ( is_single() && get_the_author_meta( 'description' ) ) :
			get_template_part( 'author-bio' );
		endif;
	?>

	<footer class="entry-footer">
		<?php twentyfifteen_entry_meta(); ?>
		<?php edit_post_link( __( 'Edit', 'twentyfifteen' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-footer -->

</article><!-- #post-## -->