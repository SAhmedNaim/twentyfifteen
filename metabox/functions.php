<?php 

function make_metabox( array $ourMetabox )
{
	$prefix = '_fifteen_';

	$ourMetabox[] = array(
		'id'			=> 'first-section',
		'title'			=> 'What is this post all about?',
		'object_types'	=> array( 'post' ),
		'fields'		=> array( 
			array(
				'name'	=> 'Type Your Framework',
				'type'	=> 'text',
				'id'	=> $prefix . 'framework'
			),
			array(
				'name'	=> 'Type your nickname',
				'type'	=> 'text_small',
				'id'	=> $prefix . 'nickname'
			),
			array(
				'name'	=> 'Type your email',
				'type'	=> 'text_email',
				'id'	=> $prefix . 'email'
			),
			array(
				'name'	=> 'Type your income',
				'type'	=> 'text_money',
				'id'	=> $prefix . 'income'
			),
			array(
				'name'	=> 'Type your address',
				'type'	=> 'textarea',
				'id'	=> $prefix . 'address'
			),
			array(
				'name'	=> 'Type your birthday',
				'type'	=> 'text_date',
				'id'	=> $prefix . 'birthday'
			),
			array(
				'name'	=> 'Type your timezone',
				'type'	=> 'select_timezone',
				'id'	=> $prefix . 'timezone'
			),
			array(
				'name'	=> 'Type a color',
				'type'	=> 'colorpicker',
				'default'	=> '#0000ff',
				'id'	=> $prefix . 'color'
			),
			array(
				'name'	=> 'Select your gender',
				'type'	=> 'radio_inline',
				'id'	=> $prefix . 'gender',
				'options'	=> array(
					'1'		=> 'Male', // key is for website
					'2'		=> 'Female'	// value is for admin panel
				)
			),
			array(
				'name'		=> 'Select your category',
				'type'		=> 'taxonomy_radio',
				'id'		=> $prefix . 'my-category',
				'taxonomy'	=> 'category'
			),
			array(
				'name'		=> 'Select capital city of Bangladesh',
				'type'		=> 'select',
				'id'		=> $prefix . 'capital-city',
				'options'	=> array(
					'1'		=> 'Dhaka',
					'2'		=> 'Kolkata',
					'3'		=> 'Mumbai',
					'4'		=> 'Thailand'
				)
			),
			array(
				'name'		=> 'Select tag',
				'type'		=> 'taxonomy_select',
				'id'		=> $prefix . 'my-tag',
				'taxonomy'	=> 'post_tag'
			),
			array(
				'name'		=> 'Select languages',
				'type'		=> 'multicheck',
				'id'		=> $prefix . 'languages',
				'options'	=> array(
					'PHP'	=> 'PHP',
					'JAVA'	=> 'JAVA',
					'C++'	=> 'C++',
					'ASP'	=> 'ASP',
					'R'		=> 'R'
				)
			),
			array(
				'name'		=> 'Select category',
				'type'		=> 'taxonomy_multicheck',
				'id'		=> $prefix . 'your_category',
				'taxonomy'	=> 'category'
			),
			array(
				'name'		=> 'Publish your content here',
				'type'		=> 'wysiwyg',
				'id'		=> $prefix . 'publish_content',
				'options'	=> array(
					'textarea_rows'	=> get_option('default_post_edit_rows', 14)
				)
			),
			array(
				'name'		=> 'Upload another image',
				'type'		=> 'file',
				'id'		=> $prefix . 'another_image',
			),
			array(
				'name'	=> 'Gallery',
				'type'	=> 'file_list',
				'id'	=> $prefix . 'gallery'
			)
		)
	);

	return $ourMetabox;
}
add_filter('cmb2_meta_boxes', 'make_metabox');
